<?php

require "vendor/autoload.php";
require "GmailParser.php";

// read the configuration file
$dataDir = getenv('KBC_DATADIR') . DIRECTORY_SEPARATOR;
$configFile = $dataDir . 'config.json';
$config = json_decode(file_get_contents($configFile), true);

$extractor = new GmailParser($config['parameters']);
$extractor->extract();

$outFile = new \Keboola\Csv\CsvFile(
    $dataDir . 'out' . DIRECTORY_SEPARATOR . 'tables' . DIRECTORY_SEPARATOR . 'destination.csv'
);

$outFile->writeRow($extractor->getHeader());

foreach ($extractor->getData(true) as $record){
    $outFile->writeRow($record);
}

try {

} catch (InvalidArgumentException $e) {
    echo $e->getMessage();
    exit(1);
} catch (Throwable $e) {
    echo $e->getMessage();
    exit(2);
}