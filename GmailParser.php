<?php
require_once 'vendor/autoload.php';
/**
 * Created by PhpStorm.
 * User: ignath
 * Date: 20.09.2017
 * Time: 20:08
 */
class GmailParser
{
    private $gmailUser = 'me';
    private $gmailParams = [];
    private $filenamePattern = '';
    private $dataHeader = '';
    private $dataFooter = '';
    private $skipPattern = '';
    private $uniquePattern = [];
    private $mergeFields = [];
    public $data = [];
    private $exportHeader = [];
    private $csvOptions = ['delimiter' => ','];
    private $importLabel;
    private $accessClient;
    private $accessToken;
    private $timeShift = -86400;

    /**
     * @return int
     */
    public function getTimeShift()
    {
        return $this->timeShift;
    }

    /**
     * @param int $timeShift
     */
    public function setTimeShift($timeShift)
    {
        $this->timeShift = $timeShift;
    }

    /**
     * @return mixed
     */
    public function getAccessClient()
    {
        return $this->accessClient;
    }

    /**
     * @param mixed $accessClient
     */
    public function setAccessClient($accessClient)
    {
        $this->accessClient = $accessClient;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return mixed
     */
    public function getImportLabel()
    {
        return $this->importLabel;
    }

    /**
     * @param mixed $importLabel
     */
    public function setImportLabel($importLabel)
    {
        $this->importLabel = $importLabel;
    }

    /**
     * @return array
     */
    public function getCsvOptions()
    {
        return $this->csvOptions;
    }

    /**
     * @param array $csvOptions
     */
    public function setCsvOptions($csvOptions)
    {
        $this->csvOptions = $csvOptions;
    }

    /**
     * @return array
     */
    public function getExportHeader()
    {
        return $this->exportHeader;
    }

    /**
     * @param array $exportHeader
     */
    public function setExportHeader($exportHeader)
    {
        $this->exportHeader = $exportHeader;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getGmailUser()
    {
        return $this->gmailUser;
    }

    /**
     * @param string $gmailUser
     */
    public function setGmailUser($gmailUser)
    {
        $this->gmailUser = $gmailUser;
    }

    /**
     * @return array
     */
    public function getGmailParams()
    {
        return $this->gmailParams;
    }

    /**
     * @param array $gmailParams
     */
    public function setGmailParams($gmailParams)
    {
        $this->gmailParams = $gmailParams;
    }

    /**
     * @return string
     */
    public function getFilenamePattern()
    {
        return $this->filenamePattern;
    }

    /**
     * @param string $filenamePattern
     */
    public function setFilenamePattern($filenamePattern)
    {
        $this->filenamePattern = $filenamePattern;
    }

    /**
     * @return string
     */
    public function getDataHeader()
    {
        return $this->dataHeader;
    }

    /**
     * @param string $dataHeader
     */
    public function setDataHeader($dataHeader)
    {
        $this->dataHeader = $dataHeader;
    }

    /**
     * @return string
     */
    public function getDataFooter()
    {
        return $this->dataFooter;
    }

    /**
     * @param string $dataFooter
     */
    public function setDataFooter($dataFooter)
    {
        $this->dataFooter = $dataFooter;
    }

    /**
     * @return string
     */
    public function getSkipPattern()
    {
        return $this->skipPattern;
    }

    /**
     * @param string $skipPattern
     */
    public function setSkipPattern($skipPattern)
    {
        $this->skipPattern = $skipPattern;
    }

    /**
     * @return array
     */
    public function getUniquePattern()
    {
        return $this->uniquePattern;
    }

    /**
     * @param array $uniquePattern
     */
    public function setUniquePattern($uniquePattern)
    {
        $this->uniquePattern = $uniquePattern;
    }

    /**
     * @return array
     */
    public function getMergeFields()
    {
        return $this->mergeFields;
    }

    /**
     * @param array $mergeFields
     */
    public function setMergeFields($mergeFields)
    {
        $this->mergeFields = $mergeFields;
    }

    public function __construct($config = [])
    {
        if(array_key_exists('gmailUser', $config)){
            $this->setGmailUser($config['gmailUser']);
        }
        if(array_key_exists('gmailParams', $config)){
            $this->setGmailParams($config['gmailParams']);
        }
        if(array_key_exists('filenamePattern', $config)){
            $this->setFilenamePattern($config['filenamePattern']);
        }
        if(array_key_exists('dataHeader', $config)){
            $this->setDataHeader($config['dataHeader']);
        }
        if(array_key_exists('dataFooter', $config)){
            $this->setDataFooter($config['dataFooter']);
        }
        if(array_key_exists('skipPattern', $config)){
            $this->setSkipPattern($config['skipPattern']);
        }
        if(array_key_exists('uniquePattern', $config)){
            $this->setUniquePattern($config['uniquePattern']);
        }
        if(array_key_exists('mergeFields', $config)){
            $this->setMergeFields($config['mergeFields']);
        }
        if(array_key_exists('csvOptions', $config)){
            $this->setCsvOptions($config['csvOptions']);
        }
        if(array_key_exists('exportHeader', $config)) {
            $this->setExportHeader($config['exportHeader']);
        }
        if(array_key_exists('importLabel', $config)) {
            $this->setImportLabel($config['importLabel']);
        }
        if(array_key_exists('client', $config)) {
            $this->setAccessClient($config['client']);
        }
        if(array_key_exists('token', $config)) {
            $this->setAccessToken($config['token']);
        }
        if(array_key_exists('timeShift', $config)) {
            $this->setTimeShift((int)$config['timeShift']);
        }
    }

    /**
     * Returns an authorized API client.
     * @return Google_Client the authorized client object
     */
    private function getClient() {
        $client = new Google_Client();
        $client->setApplicationName('Gmail Attachment Parser');
        $client->setScopes(Google_Service_Gmail::GMAIL_READONLY);
        $client->setAuthConfig($this->getAccessClient());
        $client->setAccessType('offline');
        //$client->setApprovalPrompt('force'); //helps if token is not refreshing
        $credentialsPath = './token.json';

        $client->setAccessToken($this->getAccessToken());

        if ($client->isAccessTokenExpired()) {
            $refreshTokenSaved = $client->getRefreshToken();
            $client->fetchAccessTokenWithRefreshToken($refreshTokenSaved);
            $accessTokenUpdated = $client->getAccessToken();
            $accessTokenUpdated['refresh_token'] = $refreshTokenSaved;
            //file_put_contents($credentialsPath, json_encode($accessTokenUpdated));
        }

        return $client;
    }

    /**
     * @param Google_Service_Gmail_Message $message
     * @return false|string
     */
    private function getReceivedDateFromMessage(Google_Service_Gmail_Message $message){
        $date = '';
        foreach ($message->getPayload()->getHeaders() as $header){
            if($header->getName() === "Received"){
                $values = explode(';', $header->getValue());
                $date = date("Y-m-d", strtotime(trim($values[1])) + $this->getTimeShift());
                break;
            }
        }
        return $date;
    }

    /**
     * @param Google_Service_Gmail_MessagePartBody $attachmentObj
     * @return resource
     */
    private function getFileFromAttachement(Google_Service_Gmail_MessagePartBody $attachmentObj)
    {
        $fp = fopen("php://memory", 'r+');
        fputs($fp, base64_decode(strtr($attachmentObj->getData(), array('-' => '+', '_' => '/'))));
        rewind($fp);
        return $fp;
    }

    /**
     * @param resource $csv
     * @param array $extras
     * @return array
     */
    private function getDataFromCsvFile($csv, $extras = [])
    {
        $csvOptions = $this->getCsvOptions();
        $skip = $this->getSkipPattern();
        do {
            $line = fgets($csv);
        } while (trim($line) !== $this->getDataHeader() && $line);

        $data = [];
        do {
            $line = fgets($csv);
            if(!empty($skip) && preg_match($skip, $line)) {
                continue;
            }
            if(trim($line) !== trim($this->getDataFooter())){
                $data[] = array_merge(explode($csvOptions['delimiter'], trim($line)), $extras);
            } else {
                break;
            }

        } while ($line);

        return $data;
    }

    /**
     * @param array $data
     * @return array
     */
    private function mergeDuplicates(Array $data) {
        $uniqueIDs = [];
        $noIDs = [];
        foreach ($data as $record) {
            $id = $this->getUniqueID($record);
            if($id !== null){
                $record[] = $id;
                if(array_key_exists($id, $uniqueIDs)){
                    $uniqueIDs[$id] = $this->mergeRecords($uniqueIDs[$id], $record);
                } else {
                    $uniqueIDs[$id] = $record;
                }
            } else {
                $record[] = '';
                $noIDs[] = $record;
            }
        }
        return array_merge(array_values($uniqueIDs), $noIDs);
    }

    /**
     * Unique ID is taken from the record line
     *  - fields are merged with |
     *  - from regular expression is used value in first ( )
     * @param $record
     * @return null|string
     */
    private function getUniqueID($record)
    {
        $uniqueID = null;
        $test = implode('|', $record);
        foreach ($this->getUniquePattern() as $pattern){
            preg_match($pattern, $test, $match);
            if(array_key_exists(1, $match) && !empty($match[1])){
                $uniqueID = $match[1];
                break;
            }
        }
        return $uniqueID;
    }

    /**
     * Performs math on array fields according to rules:
     * mergeFields = [1 => 'sum', 2 => 'avg']
     *      - additions on fields [1]
     *      - average from fields [2]
     * @param $array1
     * @param $array2
     * @return array
     */
    private function mergeRecords($array1, $array2){
        $result = $array1;
        foreach ($this->getMergeFields() as $key => $what){
            if(is_numeric($array1[$key]) && is_numeric($array2[$key])){
                switch ($what){
                    case 'avg':
                        $result[$key] = ($array1[$key] + $array2[$key]) / 2;
                        break;
                    case 'sum':
                    default:
                        $result[$key] = $array1[$key] + $array2[$key];
                        break;
                }
            }
        }
        return $result;
    }

    /**
     * @param Google_Service_Gmail $service
     * @param $labelName
     * @return null|$string
     */
    function getLabelIdFromLabel(Google_Service_Gmail $service, $labelName){
        $id = null;
        $res = $service->users_labels->listUsersLabels($this->getGmailUser());
        foreach ($res->getLabels() as $label){
            if($label->getName() === $labelName){
                $id = $label->getId();
                break;
            }
        }
        return $id;
    }

    /**
     * @param Google_Service_Gmail $service
     * @param $messageId
     * @param array $labelsToAdd
     * @param array $labelsToRemove
     * @return Google_Service_Gmail_Message
     */
    function modifyMessage(Google_Service_Gmail $service, $messageId, Array $labelsToAdd, Array $labelsToRemove = []) {
        $mods = new Google_Service_Gmail_ModifyMessageRequest();
        $mods->setAddLabelIds($labelsToAdd);
        $mods->setRemoveLabelIds($labelsToRemove);
        try {
            $message = $service->users_messages->modify($this->getGmailUser(), $messageId, $mods);
            //print 'Message with ID: ' . $messageId . ' successfully modified.';
            return $message;
        } catch (Exception $e) {
            //print 'An error occurred: ' . $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public function extract()
    {
        $client = $this->getClient();
        $service = new Google_Service_Gmail($client);

        $importLabelId = $this->getLabelIdFromLabel($service,$this->getImportLabel());
        $data = [];
        $results = $service->users_messages->listUsersMessages($this->getGmailUser(), $this->getGmailParams());

        foreach ($results->getMessages() as $msg) {

            $msgId = $msg->getId();
            $message = $service->users_messages->get($this->getGmailUser(), $msgId);

            foreach ($message->getPayload()->getParts() as $part) {
                $attachmentId = $part->getBody()->getAttachmentId();

                if($attachmentId && preg_match($this->getFilenamePattern(), $part->filename)){
                    $attachmentFile = $this->getFileFromAttachement($service->users_messages_attachments->get($this->getGmailUser(), $msgId, $attachmentId));
                    $attachmentData = $this->getDataFromCsvFile($attachmentFile, [$this->getReceivedDateFromMessage($message)]);

                    if(!empty($this->getUniquePattern())){
                        $attachmentData = $this->mergeDuplicates($attachmentData);
                    }

                    $data = array_merge($data, $attachmentData);
                    break;
                }
            }

            $this->modifyMessage($service, $msgId, [$importLabelId]);
        }
        $this->setData($data);
        return sizeof($data);
    }

    /**
     * @return array
     */
    public function getHeader()
    {
        $header = $this->getExportHeader();
        $csvOptions = $this->getCsvOptions();
        $defaultHeader = explode($csvOptions['delimiter'], $this->getDataHeader());
        array_walk($defaultHeader, 'trim');
        return sizeof($header) > 0 ? array_values($header) : $defaultHeader;
    }

    /**
     * @return array
     */
    public function getData($rearranged = false)
    {
        $header = $this->getExportHeader();
        if(sizeof($header) > 0 && $rearranged){
            $data = [];
            foreach ($this->data as $record){
                $arrangedRecord = [];
                foreach ($header as $key => $v){
                    $arrangedRecord[] = $record[(int)$key];
                }
                $data[] = $arrangedRecord;
            }
            return $data;
        } else {
            return $this->data;
        }
    }
}